FROM redis:4.0 as builder

ENV LIBDIR /usr/lib/redis/modules
ENV DEPS "python python-setuptools python-pip wget unzip build-essential"

# -e : exit on first non-zero return
# -x : log command execution
RUN set -ex;\
    deps="$DEPS";\
    apt-get update && \
    apt-get install -y --no-install-recommends $deps && \
    pip install rmtest

ADD ./rejson /rejson
WORKDIR /rejson
RUN set -ex && \
    make clean && \
    deps="$DEPS" && \
    make all -j4 && \
    make test

FROM redis:4.0
ENV LIBDIR /usr/lib/redis/modules
WORKDIR /data
RUN set -ex && \
    mkdir -p "$LIBDIR"
COPY --from=builder /rejson/src/rejson.so "$LIBDIR"

CMD ["redis-server", "--loadmodule", "/usr/lib/redis/modules/rejson.so"]

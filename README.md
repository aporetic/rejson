## README

### Pre-installation setup

After cloning this repo, run the following:

```bash
sudo apt install redis-tools && \


# initialize RedisLabsModules/rejson submodule
git submodule init
git submodule update
```

### Build container
```bash
docker build . -t rejson
```

### Run container
```bash
docker run -it --rm -p 6379:6379 rejson
```
